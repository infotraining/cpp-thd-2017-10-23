#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <future>
#include <random>
#include "raii_threads.hpp"

using namespace std;

string download(const string& url)
{
    cout << "Start of downloading " << url << " in a thread " << this_thread::get_id() << endl;

    random_device rnd;
    uniform_int_distribution<> distr(250, 2000);    

    if (url == "404")
        throw runtime_error("Error#404");

    auto delay = chrono::milliseconds(distr(rnd));
    
    this_thread::sleep_for(delay);

    return "Content of " + url;
}

void save(const string& filename, const string& content)
{
    cout << "Saving to " << filename << " in a thread " << this_thread::get_id() << endl;
    this_thread::sleep_for(3500ms);
    cout << "File " << filename << " saved in a thread " << this_thread::get_id() << endl;
}

void shared_future_demo()
{
    future<string> content_google = async(launch::async, &download, "https://google.pl");

    auto shared_content = content_google.share();

    thread thd_save{ [shared_content] {
        save("google.txt", shared_content.get());
    }};

    auto async_backup = async(launch::async, [shared_content] {
        save("google_backup.txt", shared_content.get());

        return true;
    });


    // continuations for futures
    //async_backup.then([](auto result) { if (result.get()) cout << "is done..."; });

    while(async_backup.wait_for(50ms) != future_status::ready)
        cout << "#" << flush;

    thd_save.join();
}

void packaged_task_demo()
{
    packaged_task<string ()> pt([] { return "Value from packaged_task;"; });  // 1 - prepare function
    auto fresult = pt.get_future(); // 2 - get future

    thread thd{move(pt)};
    thd.join();    

    cout << fresult.get() << endl;
}

template <typename Task>
auto spawn_task(Task&& task) -> future<decltype(task())>
{
    using ResultType = decltype(task());

    packaged_task<ResultType()> pt(std::forward<Task>(task));
    future<ResultType> fresult = pt.get_future();
    
    Raii::DetachedThread{ move(pt) }; // spawn a new thread with task

    return fresult;
}

int divide(int a, int b)
{
    if (b == 0)
        throw invalid_argument("denominator can't be zero");

    return a / b;
}

class Calculator
{
    promise<int> result_;
public:
    future<int> get_future()
    {
        return result_.get_future();
    }

    void operator()(int a, int b)
    {
        this_thread::sleep_for(1000ms);

        try
        {
            auto temp = divide(a, b);
            result_.set_value(temp);
        }
        catch(...)
        {
            result_.set_exception(current_exception());
        }        
    }
};

void promise_demo()
{
    Calculator calc;

    auto fresult = calc.get_future();

    Raii::DetachedThread{move(calc), 4, 0};

    cout << "Waiting for a result...." << endl;

    try
    {
        cout << "Div: " << fresult.get() << endl;
    }
    catch(const exception& e)
    {
        cout << "Caught an error: " << e.what() << endl;
    }
}

int main()
{
    // trap in standard - beware
    {
        async(launch::async, []{ save("one.txt", "ala");});
        async(launch::async, []{ save("two.txt", "ola");});
    }

    cout << "\n--------------------------------------" << endl;

    {
        spawn_task([]{ save("one.txt", "ala"); });
        spawn_task([]{ save("two.txt", "ola"); });
    }

    this_thread::sleep_for(10s);

    cout << "\n--------------------------------------" << endl;    

    future<string> content_google = async(launch::async, &download, "https://google.pl");
    future<string> content_gigaset = spawn_task([] { return download("https://gigaset.com");});
    future<string> content_unknown = async(
        launch::async, [] { return download("404"); });

    while(content_google.wait_for(50ms) != future_status::ready)
    {
        cout << "." << flush;
    }

    vector<future<string>> contents;

    contents.push_back(move(content_google));
    contents.push_back(move(content_gigaset));
    contents.push_back(move(content_unknown));

    for(auto& c : contents)
    {
        try
        {
            string result = c.get();
            cout << "Result: " << result << endl;
        }
        catch(const runtime_error& e)
        {
            cout << "Caught an exception: " << e.what() << endl;
        }
    }


    cout << "\n\n";

    shared_future_demo();

    cout << "\n--------------------------------------" << endl;    

    promise_demo();
}
