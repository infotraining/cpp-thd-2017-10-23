#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void background_work(int id, const string& text, chrono::milliseconds delay)
{
    cout << "THD#" << id << " has just started..."
         << " thread::id = " << this_thread::get_id() << endl;

    for (const auto& c : text)
    {
        cout << c << " ";
        this_thread::sleep_for(delay);
        cout.flush();
    }

    cout << "End of THD#" << id << endl;
}

class BackgroundTask
{
    const int id_;

public:
    BackgroundTask(int id)
        : id_{id}
    {
    }

    void operator()(const string& text, chrono::milliseconds delay)
    {
        cout << "BT#" << id_ << " has just started..."
             << " thread::id = " << this_thread::get_id() << endl;

        for (const auto& c : text)
        {
            cout << c << " ";
            this_thread::sleep_for(delay);
            cout.flush();
        }

        cout << "End of BT#" << id_ << endl;
    }
};

void spawn_a_thread()
{
    const string text = "From spawn_a_thread...";

    thread thd{&background_work, 665, text, 50ms};

    thd.detach();
}

int main()
{
    thread thd0;

    cout << "Default instance: " << thd0.get_id() << endl;

    spawn_a_thread();

    thread thd1{&background_work, 1, "hello threads", 100ms};
    thread thd2{&background_work, 2, "background work 2", 120ms};

    thread thd3{&background_work, 3, "DEAMON WORKS IN A THREAD", 1s};
    thd3.detach();
    assert(!thd3.joinable());

    BackgroundTask bt1{1};
    thread thd4{bt1, "Task1", 50ms};

    thread thd5{BackgroundTask{2}, "Task2", 75ms};

    auto delay = 200ms;
    thread thd6{[delay] { background_work(4, "Hello from Lambda", delay); }};

    this_thread::sleep_for(10s);

    thd1.join();
    assert(!thd1.joinable());

    thd2.join();
    assert(!thd2.joinable());

    thd4.join();
    thd5.join();
    thd6.join();
}
