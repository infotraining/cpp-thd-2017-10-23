#include <algorithm>
#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <numeric>
#include <thread>
#include <vector>
#include <atomic>
#include <condition_variable>

#include "raii_threads.hpp"

using namespace std;

namespace WithMutex
{
    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
        mutex mtx_ready_;

    public:
        void read()
        {
            cout << "Start reading in a thread::id " << this_thread::get_id() << endl;

            data_.resize(100);
            iota(data_.begin(), data_.end(), 1);

            this_thread::sleep_for(1500ms);

            {
                lock_guard<mutex> lk{mtx_ready_}; // mtx_ready_.lock() - ACQUIRE ////////////////////////
                is_ready_ = true;
            } // mtx_ready_.unlock() - RELEASE ///////////////////////////////////////////////////////////
        }

        void process(int id)
        {
            bool ready_flag = false;

            do
            {
                lock_guard<mutex> lk{mtx_ready_}; // mtx_ready_.lock() - ACQUIRE ////////////////////////
                ready_flag = is_ready_;
            } // mtx_ready_.unlock() - RELEASE ///////////////////////////////////////////////////////////
            while (!ready_flag);

            cout << "Processing data in a thread::id " << this_thread::get_id() << endl;
            auto result = accumulate(data_.begin(), data_.end(), 0);

            cout << "Result: " << result << endl;
        }
    };
}

namespace WithAtomics
{
    class Data
    {
        vector<int> data_;
        atomic<bool> is_ready_{false};

    public:
        void read()
        {
            cout << "Start reading in a thread::id " << this_thread::get_id() << endl;

            data_.resize(100);
            iota(data_.begin(), data_.end(), 1);

            this_thread::sleep_for(1500ms);
            
            is_ready_.store(true, memory_order_release); //////////////////////////////////////  Release Semantics            
        }

        void process(int id)
        {
            while (!is_ready_.load(memory_order_acquire)) ////////////////////////////////////// Acquire Semantics
                continue;

            cout << "Processing data in a thread::id " << this_thread::get_id() << endl;
            auto result = accumulate(data_.begin(), data_.end(), 0);

            cout << "Result: " << result << endl;
        }
    };
}

namespace WithConditionVariable
{
    class Data
    {
        vector<int> data_;
        bool is_ready_{false};
        mutex mtx_ready_;
        condition_variable cv_ready_;

    public:
        void read()
        {
            cout << "Start reading in a thread::id " << this_thread::get_id() << endl;

            data_.resize(100);
            iota(data_.begin(), data_.end(), 1);

            this_thread::sleep_for(1500ms);

            {
                lock_guard<mutex> lk{mtx_ready_};
                is_ready_ = true;
            }

            cv_ready_.notify_all();
        }

        void process(int id)
        {
            {
                unique_lock<mutex> lk{mtx_ready_};
                cv_ready_.wait(lk, [this] { return is_ready_; });
            }
            
            cout << "Processing data in a thread::id " << this_thread::get_id() << endl;
            auto result = accumulate(data_.begin(), data_.end(), 0);

            cout << "Result: " << result << endl;
        }
    };
}

int main()
{
    WithConditionVariable::Data data;

    Raii::JoiningThread producer{[&data] { data.read(); }};
    Raii::JoiningThread consumer1{[&data] { data.process(1); }};
    Raii::JoiningThread consumer2{[&data] { data.process(2); }};
}
