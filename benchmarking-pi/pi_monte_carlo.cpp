#include <celero/Celero.h>

#include <algorithm>
#include <atomic>
#include <cstdint>
#include <future>
#include <iostream>
#include <mutex>
#include <numeric>
#include <random>
#include <thread>

#include "raii_threads.hpp"

CELERO_MAIN

using namespace std;

constexpr int N = 10'000'000;
constexpr int no_of_samples = 1;
constexpr int no_of_iterations = 10;

using counter_t = unsigned long long;

using Point = pair<double, double>;

class SpinLock
{
    std::atomic_flag is_taken_{false};
public:
    void lock()
    {
        while(is_taken_.test_and_set(memory_order_acquire))
            continue;
    }

    void unlock()
    {   
        is_taken_.clear(memory_order_release);
    }

    bool try_lock()
    {
        return is_taken_.test_and_set(memory_order_acquire);
    }
};

class RndPoint
{
    mt19937_64 rnd_;
    uniform_real_distribution<double> distr_;
public:
    RndPoint() : rnd_(random_device()()), distr_{-1.0, 1.0}
    {}

    Point operator()() 
    {
        return make_pair(distr_(rnd_), distr_(rnd_));
    }
};

counter_t calc_hits(counter_t n)
{
    RndPoint rnd_point;

    counter_t hits{};

    for(counter_t i = 1; i <= n; i++)
    {           
        double x, y;
        tie(x, y) = rnd_point();            

        if(x*x + y*y <= 1)
        {
            hits++;
        }
    }

    return hits;
}


void calc_hits(counter_t n, counter_t& hits)
{
    RndPoint rnd_point;

    for(counter_t i = 1; i <= n; i++)
    {           
        double x, y;
        tie(x, y) = rnd_point();            

        if(x*x + y*y <= 1)
        {
            hits++;
        }
    }
}

template <typename MutexType>
struct SynchronizedCounter
{
    using mutex_type = MutexType;
    MutexType mtx_counter;
    counter_t counter{};
};

template <typename SynchronizedCounterType>
void calc_hits(counter_t n, SynchronizedCounterType& hits)
{
    RndPoint rnd_point;    

    for(counter_t i = 1; i <= n; i++)
    {           
        double x, y;
        tie(x, y) = rnd_point();            

        if(x*x + y*y <= 1)
        {
            lock_guard<typename SynchronizedCounterType::mutex_type> lk{hits.mtx_counter};
            ++hits.counter; 
        }
    }            
}

void calc_hits(counter_t n, atomic<counter_t>& hits)
{
    RndPoint rnd_point;
    
    counter_t tmp_hits = 0;

        for(counter_t i = 1; i <= n; i++)
        {           
            double x, y;
            tie(x, y) = rnd_point();            
    
            if(x*x + y*y <= 1)
            {
                ++tmp_hits;            
            }
        }
    
    hits.fetch_add(tmp_hits, memory_order_relaxed); // hits += tmp_hits
}

void calc_hits_with_local_counter(counter_t n, counter_t& hits)
{
    RndPoint rnd_point;
    counter_t tmp_hits = 0;

    for(counter_t i = 1; i <= n; i++)
    {           
        double x, y;
        tie(x, y) = rnd_point();            

        if(x*x + y*y <= 1)
        {
            tmp_hits++;
        }
    }

    hits += tmp_hits;
}

double calc_pi_single_thread(counter_t throws)
{
    counter_t hits = 0;
    calc_hits(throws, hits);

    return 4.0 * (static_cast<double>(hits) / throws);    
}

double calc_pi_multithreading_false_sharing(counter_t throws)
{
    size_t hardware_threads_count = max(1u, thread::hardware_concurrency());
    counter_t throws_per_thread = throws / hardware_threads_count;

    vector<counter_t> hits_per_thread(hardware_threads_count);
    
    {
        vector<Raii::JoiningThread> threads;

        for(size_t i = 0; i < hardware_threads_count; ++i)
        {
            threads.emplace_back([&hits_per_thread, i, throws_per_thread] { 
                calc_hits(throws_per_thread, hits_per_thread[i]); });
        }
    } // joining threads       

    return 4.0 * (accumulate(begin(hits_per_thread), end(hits_per_thread), 0.0) / throws);;
}

double calc_pi_multithreading_with_local_counter(counter_t throws)
{
    size_t hardware_threads_count = max(1u, thread::hardware_concurrency());
    counter_t throws_per_thread = throws / hardware_threads_count;

    vector<counter_t> hits_per_thread(hardware_threads_count);
    
    {
        vector<Raii::JoiningThread> threads;

        for(size_t i = 0; i < hardware_threads_count; ++i)
        {
            threads.emplace_back([&hits_per_thread, i, throws_per_thread] { 
                calc_hits_with_local_counter(throws_per_thread, hits_per_thread[i]); });
        }
    } // joining threads       

    return 4.0 * (accumulate(begin(hits_per_thread), end(hits_per_thread), 0.0) / throws);;
}

template <typename MutexType>
double calc_pi_multithreading_with(counter_t throws)
{
    size_t hardware_threads_count = max(1u, thread::hardware_concurrency());
    counter_t throws_per_thread = throws / hardware_threads_count;

    SynchronizedCounter<MutexType> hits;
    
    {
        vector<Raii::JoiningThread> threads;

        for(size_t i = 0; i < hardware_threads_count; ++i)
        {
            threads.emplace_back([&hits, throws_per_thread] { 
                calc_hits(throws_per_thread, hits); });
        }
    } // joining threads       

    return 4.0 * (static_cast<double>(hits.counter) / throws);;
}

double calc_pi_multithreading_with_atomic(counter_t throws)
{
    size_t hardware_threads_count = max(1u, thread::hardware_concurrency());
    counter_t throws_per_thread = throws / hardware_threads_count;

    atomic<counter_t> hits;    
    
    {
        vector<Raii::JoiningThread> threads;

        for(size_t i = 0; i < hardware_threads_count; ++i)
        {
            threads.emplace_back([&hits, throws_per_thread] { 
                calc_hits(throws_per_thread, hits); });
        }
    } // joining threads       

    return 4.0 * (static_cast<double>(hits.load(memory_order_relaxed)) / throws);;
}

double calc_pi_with_futures(counter_t throws)
{
    size_t hardware_threads_count = max(1u, thread::hardware_concurrency());
    counter_t throws_per_thread = throws / hardware_threads_count;

    vector<future<counter_t>> partial_hits;

    for(size_t i  = 0; i < hardware_threads_count; ++i)
    {
        partial_hits.push_back(async(launch::async, [throws_per_thread] { 
            return calc_hits(throws_per_thread); }));
    }

    auto hits = accumulate(begin(partial_hits), end(partial_hits), 0, [](auto h, auto& f) { return h + f.get(); });

    return 4.0 * (static_cast<double>(hits) / throws);
}

BASELINE(MonteCarloPi, SingleThreaded, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_single_thread(N));
}

BENCHMARK(MonteCarloPi, MultThdFS, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_multithreading_false_sharing(N));
}

BENCHMARK(MonteCarloPi, MultThdLC, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_multithreading_with_local_counter(N));
}

BENCHMARK(MonteCarloPi, MultThdMutex, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_multithreading_with<mutex>(N));
}

BENCHMARK(MonteCarloPi, MultThdSpinLock, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_multithreading_with<SpinLock>(N));
}

BENCHMARK(MonteCarloPi, MultThdAtomic, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_multithreading_with_atomic(N));
}

BENCHMARK(MonteCarloPi, MultFutures, no_of_samples, no_of_iterations)
{
    celero::DoNotOptimizeAway(calc_pi_with_futures(N));
}

