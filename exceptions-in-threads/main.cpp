#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <vector>

#include "raii_threads.hpp"

using namespace std;

void background_work(int id, const string& text, chrono::milliseconds delay, exception_ptr& excpt)
{
    try
    {
        cout << "THD#" << id << " has just started..."
             << " thread::id = " << this_thread::get_id() << endl;

        if (text == "Error")
            throw runtime_error("Exception from a THD#" + to_string(id));

        for (const auto& c : text)
        {
            cout << c << " ";
            this_thread::sleep_for(delay);
            cout.flush();
        }

        cout << "End of THD#" << id << endl;
    }
    catch (...)
    {
        cout << "Caught an exception in a thread::id: " << this_thread::get_id() << endl;
        excpt = current_exception();
    }
}

int main()
{
    auto start = chrono::high_resolution_clock::now();

    using namespace Raii;

    const size_t no_of_threads = max(1u, thread::hardware_concurrency());

    cout << "Hardware threads count: " << no_of_threads << endl;

    vector<exception_ptr> eptrs(no_of_threads);
    
    {
        vector<JoiningThread> thds(no_of_threads);

        for(size_t i = 0; i < no_of_threads; ++i)
            thds[i] = JoiningThread{&background_work, i, "Error", 100ms, ref(eptrs[i])};
    } // join on threads


    for(auto& e : eptrs)
    {
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch (const runtime_error& e)
            {
                cout << "Handling an " << e.what() << " in main()" << endl;
            }
        }
    }    

    auto end = chrono::high_resolution_clock::now();

    auto interval = chrono::duration_cast<chrono::microseconds>(end - start);

    cout << "Time elapsed: " << interval.count() << "us" << endl;
}