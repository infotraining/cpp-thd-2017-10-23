#include <iostream>
#include <mutex>

#include "raii_threads.hpp"

class BankAccount
{
    const int id_;
    double balance_;
    using mutex_type = std::recursive_mutex;
    mutable mutex_type mtx_;

public:
    BankAccount(int id, double balance)
        : id_(id)
        , balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::lock(mtx_, to.mtx_); // locking avoiding a deadlock

        std::unique_lock<mutex_type> lk_from{mtx_, std::adopt_lock};
        std::unique_lock<mutex_type> lk_to{to.mtx_, std::adopt_lock};

        withdraw(amount);
        to.deposit(amount);
        // balance_ -= amount;
        // to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex_type> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex_type> lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }

    bool try_lock()
    {
        return mtx_.try_lock();
    }
};

namespace Proxy
{
    class BankAccount
    {
        const int id_;
        double balance_;

    public:
        BankAccount(int id, double balance)
            : id_(id)
            , balance_(balance)
        {
        }

        void print() const
        {
            std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
        }

        void transfer(BankAccount& to, double amount)
        {
            balance_ -= amount;
            to.balance_ += amount;
        }

        void withdraw(double amount)
        {
            balance_ -= amount;
        }

        void deposit(double amount)
        {
            balance_ += amount;
        }

        int id() const
        {
            return id_;
        }

        double balance() const
        {
            return balance_;
        }
    };

    class ProxyBankAccount
    {
        BankAccount account_;
        using mutex_type = std::recursive_mutex;
        mutable mutex_type mtx_;

    public:
        ProxyBankAccount(int id, double balance)
            : account_{id, balance}
        {
        }

        void print() const
        {
            std::lock_guard<mutex_type> lk{mtx_};
            account_.print();
        }

        void transfer(ProxyBankAccount& to, double amount)
        {
            std::unique_lock<mutex_type> lk_from{mtx_, std::defer_lock};
            std::unique_lock<mutex_type> lk_to{to.mtx_, std::defer_lock};

            std::lock(lk_from, lk_to);

            account_.transfer(to.account_, amount);
        }

        void withdraw(double amount)
        {
            std::lock_guard<mutex_type> lk{mtx_};
            account_.withdraw(amount);
        }

        void deposit(double amount)
        {
            std::lock_guard<mutex_type> lk{mtx_};
            account_.deposit(amount);
        }

        int id() const
        {
            std::lock_guard<mutex_type> lk{mtx_};
            return account_.id();
        }

        double balance() const
        {
            std::lock_guard<mutex_type> lk{mtx_};
            return account_.balance();
        }
    };
}

namespace PolicyBasedDesign
{
    // ThreadingPolicy for single threaded code
    struct SingleThreaded
    {
        struct EmptyLockable
        {
            void lock() {}
            void unlock() {}
        };

        using mutex_type = EmptyLockable;

    protected:
        mutable mutex_type mtx_;
    };


    struct MultiThreaded
    {
        using mutex_type = std::recursive_mutex;

        void lock()
        {
            mtx_.lock();
        }

        void unlock()
        {
            mtx_.unlock();
        }

        bool try_lock()
        {
            return mtx_.try_lock();
        }

    protected:
        mutable mutex_type mtx_;        
    };


    template <typename ThreadingPolicy>
    class BankAccount : public ThreadingPolicy
    {
        const int id_;
        double balance_;
        using mutex_type = typename ThreadingPolicy::mutex_type;
    public:
        BankAccount(int id, double balance)
            : id_(id)
            , balance_(balance)
        {
        }

        void print() const
        {
            std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
        }

        void transfer(BankAccount& to, double amount)
        {
            std::lock(ThreadingPolicy::mtx_, to.mtx_);

            std::lock_guard<mutex_type> lk_from{ThreadingPolicy::mtx_, std::adopt_lock};
            std::lock_guard<mutex_type> lk_to{to.ThreadingPolicy::mtx_, std::adopt_lock};

            balance_ -= amount;
            to.balance_ += amount;
        }

        void withdraw(double amount)
        {
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            balance_ -= amount;
        }

        void deposit(double amount)
        {
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            balance_ += amount;
        }

        int id() const
        {
            return id_;
        }

        double balance() const
        {
            std::lock_guard<mutex_type> lk{ThreadingPolicy::mtx_};
            return balance_;
        }
    };

}

template <typename AccountType>
void make_deposits(AccountType& ba, size_t count, double amount)
{
    for (size_t i = 0; i < count; ++i)
        ba.deposit(amount);
}

template <typename AccountType>
void make_withdraws(AccountType& ba, size_t count, double amount)
{
    for (size_t i = 0; i < count; ++i)
        ba.withdraw(amount);
}

template <typename AccountType>
void make_transfers(AccountType& from, AccountType& to, size_t count, double amount)
{
    for (size_t i = 0; i < count; ++i)
        from.transfer(to, amount);
}

void using_policy_based_design()
{
    // single threaded code
    using AccountST = PolicyBasedDesign::BankAccount<PolicyBasedDesign::SingleThreaded>;

    AccountST ba1(1, 100.0);
    ba1.deposit(100.0);

    // multithreaded code
    using AccountMT = PolicyBasedDesign::BankAccount<PolicyBasedDesign::MultiThreaded>;
    AccountMT ba2(2, 1400.0);

    {
        std::lock_guard<AccountMT> lk{ba2};
        ba2.deposit(400.0);
        ba2.withdraw(50.0);
    }
}

int main()
{
    using namespace std;

    Proxy::ProxyBankAccount ba1(1, 10000);
    Proxy::ProxyBankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    const size_t no_of_transactions = 1'000'000;

    {
        Raii::JoiningThread thd1{ [&ba1, no_of_transactions] { make_deposits(ba1, no_of_transactions, 1.0); } };
        Raii::JoiningThread thd2{ [&ba1, no_of_transactions] { make_withdraws(ba1, no_of_transactions, 1.0); } };
    }

    std::cout << "After joining threads:" << std::endl;
    ba1.print();
    ba2.print();

    {
        Raii::JoiningThread thd1{ [&ba1, &ba2, no_of_transactions] {
            make_transfers( ba1, ba2, no_of_transactions, 1.0); } 
        };

        Raii::JoiningThread thd2{[&ba1, &ba2, no_of_transactions] {
            make_transfers(ba2, ba1, no_of_transactions, 1.0); }
        };

        // should be a transaction
        // {
        //     unique_lock<BankAccount> lk1{ba1, defer_lock};
        //     unique_lock<BankAccount> lk2{ba2, defer_lock};

        //     std::lock(lk1, lk2);

        //     ba1.deposit(100.0);
        //     ba1.withdraw(400.0);
        //     ba1.transfer(ba2, 1000.0);
        // }
    }

    std::cout << "After transfering threads:" << std::endl;
    ba1.print();
    ba2.print();
}
