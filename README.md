# README #

### Doc ###

* https://infotraining.bitbucket.io/cpp-thd/


### Celero dependencies ###

* https://github.com/DigitalInBlue/Celero
* sudo apt-get install libncurses5-dev


### Links ###

* compiler explorer - https://gcc.godbolt.org/

### Books ###

* Discovering Modern C++: An Intensive Course for Scientists, Engineers, and Programmers - Peter Gottschling
* The C++ Standard Library: A Tutorial and Reference, Second Edition -  Nicolai M. Josuttis 

### Ankieta ###

* https://docs.google.com/forms/d/e/1FAIpQLSe_OQFRcon-09L7h0XxpxoA88GlSYFlPv5VmIp1nCmpp3XUGQ/viewform?hl=pl
