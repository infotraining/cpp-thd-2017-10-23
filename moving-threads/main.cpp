#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

#include "raii_threads.hpp"

using namespace std;

void background_work(int id, const string& text, chrono::milliseconds delay)
{
    cout << "THD#" << id << " has just started..."
         << " thread::id = " << this_thread::get_id() << endl;

    for (const auto& c : text)
    {
        cout << c << " ";
        this_thread::sleep_for(delay);
        cout.flush();
    }

    cout << "End of THD#" << id << endl;
}

thread spawn_a_thread(const string& text)
{
    static int id = 100;

    thread thd{&background_work, id++, text, 50ms};

    return thd;
}

void may_throw()
{
    throw runtime_error("Error#13");
}



string full_name(string fn, string ln)
{
    return fn + " " + ln;
}

// void universal_ref()
// {
//     string str = "Text";
//     auto&& ref_lstr = str; // string&
//     auto&& ref_rstr = full_name("Jan", "Kowalski"); // string&&

//     auto&& item = 3.14; // double&&
// }

int main() try
{
    Raii::JoiningThread thd1{&background_work, 1, "text", 100ms };
    //Raii::JoiningThread thd_cpy = thd1;  // JoiningThread is non-copyable
    
    thread thd2 = move(thd1.get());

    thread thd3 = spawn_a_thread("concurrent");

    vector<Raii::JoiningThread> thds;

    thds.push_back(thread{&background_work, 2, "from vector", 30ms});
    thds.emplace_back(&background_work, 3, "emplaced back", 40ms);
    thds.push_back(spawn_a_thread("text"));
    thds.push_back(move(thd2));
    thds.push_back(move(thd3));

    may_throw();    

    const vector<int> data = { 1, 2, 3, 4, 5, 6 };

    vector<int> target1;
    vector<int> target2;

    {        
        Raii::JoiningThread cpy_thd1{[&data, &target1] { 
            copy(begin(data), end(data), back_inserter(target1)); }};

        Raii::JoiningThread cpy_thd2{[&data, &target2] { 
            copy(begin(data), end(data), back_inserter(target2)); }};
    }

    cout << "target1: ";
    for(const auto& item : target1)
        cout << item << " ";
    cout << endl;

    cout << "target2: ";
    for(const auto& item : target2)
        cout << item << " ";
    cout << endl;
}
catch(const runtime_error& e)
{
    cout << "Caught an error: " << e.what() << endl;
}