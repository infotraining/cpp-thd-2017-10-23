#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <memory>

using namespace std;

class FileResource
{
    string content_;

    using iterator = string::iterator;
    using const_iterator = string::const_iterator;
public:
    FileResource(const string& txt) : content_{txt}
    {
        cout << "FileResource(" << content_ << ")" << endl;
    }

    ~FileResource()
    {
        cout << "~FileResource(" << content_ << ")" << endl;
    }

    FileResource(const FileResource&) = delete;
    FileResource& operator=(const FileResource&) = delete;

    iterator begin()
    {
        return content_.begin();
    }

    iterator end()
    {
        return content_.end();
    }

    const_iterator begin() const
    {
        return content_.begin();
    }

    const_iterator end() const
    {
        return content_.end();
    }    
};

void read_from_file(const FileResource& f)
{
    cout << "Reading from file in thd#" << this_thread::get_id() << endl;

    for(const auto& c : f)
    {
        cout << c << " ";
        this_thread::sleep_for(50ms);
        cout.flush();
    }

    cout << "End of reading in thd#" << this_thread::get_id() << endl;
}

int main()
{
    thread thd1, thd2;
    
    {
        shared_ptr<const FileResource> file = make_shared<const FileResource>("text from file");
    
        thd1 = thread{[file] { read_from_file(*file); }};
        thd2 = thread{[file] { read_from_file(*file); }};
    }

    thd1.join();
    thd2.join();
}
