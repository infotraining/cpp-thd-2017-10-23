#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>
#include "raii_threads.hpp"
#include "thread_safe_queue.hpp"

using namespace std;

void background_work(int id)
{
    cout << "BW#" << id << " has been started in thread " << this_thread::get_id() << endl;

    random_device rnd;
    uniform_int_distribution<> distr(250, 2000);

    auto delay = chrono::milliseconds(distr(rnd));
    
    this_thread::sleep_for(delay);
    cout << "BW#" << id << " has finished..." << endl;    
}

string download(const string& url)
{
    cout << "Start of downloading " << url << " in a thread " << this_thread::get_id() << endl;

    random_device rnd;
    uniform_int_distribution<> distr(250, 2000);    

    if (url == "404")
        throw runtime_error("Error#404");

    auto delay = chrono::milliseconds(distr(rnd));
    
    this_thread::sleep_for(delay);

    return "Content of " + url;
}

using Task = std::function<void()>;

class ThreadPool
{    
public:
    ThreadPool(size_t size = std::max(thread::hardware_concurrency(), 1u)) 
    {
        threads_.reserve(size);
        for(size_t i = 0; i < size; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            task_q_.push(end_of_work_);
    }

    template <typename Callable>
    auto submit(Callable&& task) -> future<decltype(task())>
    {
        using ResultType = decltype(task());

        using Package = packaged_task<ResultType()>;
        shared_ptr<Package> pt = make_shared<Package>(move(task));
        auto fresult = pt->get_future();
             
        task_q_.push([pt] { (*pt)(); });

        return fresult;
    }
private:
    const Task end_of_work_;

    // order of items matters - destructor of threads_ should be called before destructor of task_q_
    ThreadSafeQueue<Task> task_q_;    
    vector<Raii::JoiningThread> threads_;

    void run()
    {
        while(true)
        {
            Task task;
            task_q_.pop(task); 

            if (!task)
            {
                cout << "End of thread " << this_thread::get_id() << endl;
                return;
            }

            task(); // executing a task
        }
    }
};

int main()
{
    {
        ThreadPool thd_pool(20);

        thd_pool.submit([] { background_work(0); });

        for(size_t i = 1; i <= 20; ++i)
            thd_pool.submit([i] { background_work(i); });
    } // wait for finishing all tasks


    {
        vector<string> urls = { "a.com", "b.com", "c.com", "d.com", "e.pl", "f.pl", "g.com" };

        ThreadPool thd_pool(4);

        vector<future<string>> contents;

        for(const auto& url : urls)
        {
            contents.push_back(thd_pool.submit([url] { return download(url); }));
        }

        // processing of content
        for(auto& fc : contents)
        {
            cout << fc.get() << endl;
        }
    }
}
