#include <atomic>
#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <memory>

using namespace std;

class SpinLock
{
    std::atomic_flag is_taken_{false};

public:
    void lock()
    {
        while (is_taken_.test_and_set())
            continue;
    }

    void unlock()
    {
        is_taken_.clear();
    }

    bool try_lock()
    {
        return is_taken_.test_and_set();
    }
};

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

namespace ThreadUnsafe
{
    template <typename T>
    class RefCounted : boost::noncopyable
    {
        int ref_count_{0};

    public:
        RefCounted() = default;

        void add_ref()
        {
            ++ref_count_;
        }

        void release()
        {
            if (--ref_count_ == 0)
            {
                delete static_cast<T*>(this);
            }
        }
    };
}

namespace ThreadSafe
{
    template <typename T>
    class RefCounted : boost::noncopyable
    {
        atomic<int> ref_count_{0};

    public:
        RefCounted() = default;

        void add_ref()
        {
            ref_count_.fetch_add(1, memory_order_relaxed);
        }

        void release()
        {
            if (ref_count_.fetch_sub(1, memory_order_acq_rel) == 1)
            {
                delete static_cast<T*>(this);
            }
        }
    };
}

class Gadget : public ThreadSafe::RefCounted<Gadget> // CRTP
{
public:
    Gadget()
    {
        cout << "Gadget()" << endl;
    }

    ~Gadget()
    {
        cout << "~Gadget()" << endl;
    }
};

template <typename T>
void intrusive_ptr_add_ref(T* obj)
{
    obj->add_ref();
}

template <typename T>
void intrusive_ptr_release(T* obj)
{
    obj->release();
}

/////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

class ILogger
{
public:
    virtual void log(string msg) = 0;
    virtual ~ILogger() = default;
};

namespace WithAtomics
{
    class Logger : public ILogger
    {
        static atomic<Logger*> instance_;
        static mutex mtx_instance_;

    public:
        Logger(const Logger&) = delete;
        Logger& operator=(const Logger&) = delete;

        static Logger& instance()
        {
            if (!instance_) // double checked locking pattern
            {
                lock_guard<mutex> lk{mtx_instance_};
                if (!instance_)
                {
                    instance_ = new Logger();

                    // // 1 - malloc
                    // void* raw_mem = ::operator new(sizeof(Logger));
                    // // 2 - cctor
                    // new (raw_mem) Logger();
                    // // 3 - assignment
                    // instance_.store(static_cast<Logger*>(raw_mem));
                }
            }
            return *instance_;
        }

        void log(string msg)
        {
            cout << "Log: " << msg << endl;
        }

    private:
        Logger() = default;
    };
}

atomic<WithAtomics::Logger*> WithAtomics::Logger::instance_{nullptr};
mutex WithAtomics::Logger::mtx_instance_;


namespace Meyers
{
    class Logger : public ILogger
    {        
    public:
        Logger(const Logger&) = delete;
        Logger& operator=(const Logger&) = delete;

        static Logger& instance()
        {
            static Logger unique_instance;
            
            return unique_instance;
        }

        void log(string msg)
        {
            cout << "Log: " << msg << endl;
        }

    private:
        Logger() = default;
    };
}

namespace VisualStudio2012
{
    class Logger : public ILogger
    {        
        static unique_ptr<Logger> instance_;
        static once_flag init_flag_;
    public:
        Logger(const Logger&) = delete;
        Logger& operator=(const Logger&) = delete;

        static Logger& instance()
        {
            call_once(init_flag_, [] { instance_ = make_unique<Logger>(); });
            
            return *instance_;
        }

        void log(string msg)
        {
            cout << "Log: " << msg << endl;
        }

    private:
        Logger() = default;
        friend unique_ptr<Logger> make_unique<Logger>();
    };
}

unique_ptr<VisualStudio2012::Logger> VisualStudio2012::Logger::instance_;
once_flag VisualStudio2012::Logger::init_flag_;

class Service
{
public:
    void run()
    {
        ILogger& logger = get_logger();
        logger.log("run");
    }

protected:
    ILogger& get_logger()
    {
        return VisualStudio2012::Logger::instance();
    }
};

int main()
{
    thread thd1, thd2;

    {
        boost::intrusive_ptr<Gadget> ptr1(new Gadget());

        {
            boost::intrusive_ptr<Gadget> ptr2 = ptr1;
            thd1 = thread{[ptr2] { 
                Service srv;
                srv.run();
                this_thread::sleep_for(1000ms); 
            }};
        }

        thd2 = thread{[ptr1] { 
            Service srv;
            srv.run();
            this_thread::sleep_for(1000ms); }
        };
    }

    thd1.join();
    thd2.join();

    cout << "End of main..." << endl;
}