#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <condition_variable>
#include <mutex>
#include <queue>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> q_;
    mutable std::mutex mtx_q_;
    std::condition_variable cv_not_empty_;
public:
    bool empty() const
    {
        std::lock_guard<std::mutex> lk{mtx_q_};
        return q_.empty();
    }

    void push(const T& item)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_q_};
            q_.push(item);                
        }
        cv_not_empty_.notify_one();
    }

    void push(T&& item)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_q_};
            q_.push(std::move(item));                
        }
        cv_not_empty_.notify_one();
    }

    void push(std::initializer_list<T> items)
    {
        {
            std::lock_guard<std::mutex> lk{mtx_q_};
            for(const auto& item : items)
                q_.push(item);                
        }

        cv_not_empty_.notify_all();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_q_};
        cv_not_empty_.wait(lk, [this] { return not q_.empty();});

        pop_from_q(item);        
    }

    bool try_pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_q_, std::try_to_lock}; // non-blocking constructor
        if (lk.owns_lock() and not q_.empty())
        {
            pop_from_q(item);
            return true;
        }

        return false;
    }
private:
    void pop_from_q(T& item)
    {
#if __cplusplus >= 201703L
        
        if constexpr(std::is_nothrow_move_assignable<T>::value) 
        {
            item = std::move(q_.front());
        }
        else
        {
            item = q_.front();
        }        
#else
        item = q_.front();    
#endif

        q_.pop();
    }
};

#endif // THREAD_SAFE_QUEUE_HPP
