#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

#include "thread_safe_queue.hpp"
#include "raii_threads.hpp"

using namespace std;

namespace Before
{
    class Logger
    {
        ofstream fout_;

    public:
        Logger(const string& file_name)
        {
            fout_.open(file_name);
        }

        Logger(const Logger&) = delete;
        Logger& operator=(const Logger&) = delete;

        ~Logger()
        {
            fout_.close();
        }

        void log(const string& message)
        {
            fout_ << message << endl;
            fout_.flush();
        }
    };
}

namespace After
{
    namespace WithMutex
    {
        class Logger
        {
            ofstream fout_;
            std::mutex mtx_;

        public:
            Logger(const string& file_name)
            {
                fout_.open(file_name);
            }

            Logger(const Logger&) = delete;
            Logger& operator=(const Logger&) = delete;

            ~Logger()
            {
                fout_.close();
            }

            void log(const string& message)
            {
                std::lock_guard<std::mutex> lk{mtx_};
                fout_ << message << endl;
                fout_.flush();
            }
        };
    }

    inline namespace WithActiveObject
    {
        using Task = function<void()>;

        class ActiveObject
        {
            ThreadSafeQueue<Task> q_;
            Raii::JoiningThread thd_;
            bool end_of_work_ = false;
        public:
            ActiveObject() 
            {
                thd_ = Raii::JoiningThread([this] { run(); });
            }

            ~ActiveObject()
            {
                submit([this] { end_of_work_ = true; });
            }


            ActiveObject(const ActiveObject&) = delete;
            ActiveObject& operator=(const ActiveObject&) = delete;

            void submit(Task task)
            {
                q_.push(task);
            }

        private:
            void run()
            {
                while(true)
                {
                    Task task;
                    q_.pop(task);
                    task(); // execution of task in active object's thread

                    if (end_of_work_)
                        return;
                }
            }
        };

        class Logger
        {
            ofstream fout_;
            ActiveObject ao_;
    
        public:
            Logger(const string& file_name)
            {
                fout_.open(file_name);
            }
    
            Logger(const Logger&) = delete;
            Logger& operator=(const Logger&) = delete;
        
            void log(const string& message)
            {
                ao_.submit([this, message] { do_log(message); });
            }
        private:
            void do_log(const string& message)
            {
                fout_ << message << endl;
                fout_.flush();
            }
        };
    }
}

using namespace After;

void run(Logger& logger, int id)
{
    for (int i = 0; i < 1000; ++i)
        logger.log("Log#" + to_string(id) + " - Event#" + to_string(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();
}
