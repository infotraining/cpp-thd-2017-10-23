#include "thread_safe_queue.hpp"
#include <iostream>
#include <memory>

using namespace std;

class FunctionWrapper
{
    struct ImplBase
    {
        virtual void call() = 0;
        virtual ~ImplBase() {}
    };

    unique_ptr<ImplBase> impl_;

    template <typename F>
    struct ImplType : public ImplBase
    {
        F f;

        ImplType(F&& f) : f(move(f))
        {
        }

        void call() override
        {
            f(); // delegation
        }
    };

public:
    template <typename F>
    FunctionWrapper(F&& f) : impl_{new ImplType<F>(move(f))}
    {
    }

    void operator()()
    {
        impl_->call();
    }

    FunctionWrapper() = default;
    FunctionWrapper(FunctionWrapper&& source) = default;
    FunctionWrapper& operator=(FunctionWrapper&&) = default;

    FunctionWrapper(const FunctionWrapper& source) = delete;
    FunctionWrapper& operator=(const FunctionWrapper&) = delete;

    bool operator==(nullptr_t ptr)
    {
        return impl_.get() == ptr;
    }
};

typedef FunctionWrapper Task;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads)
    {
        for (size_t i = 0; i < no_of_threads; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for (size_t i = 0; i < threads_.size(); ++i)
            submit(END_OF_WORK);

        for (auto& t : threads_)
            t.join();
    }

    void submit(Task f)
    {
        task_queue_.push(f);
    }

private:
    ThreadSafeQueue<Task> task_queue_;
    std::vector<std::thread> threads_;

    const static nullptr_t END_OF_WORK;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (task == END_OF_WORK)
                return;

            task();
        }
    }

    bool is_end_of_work(Task t)
    {
        return t == nullptr;
    }
};

const nullptr_t ThreadPool::END_OF_WORK = nullptr;

void background_task(int id)
{
    cout << "BT#" << id << " starts..." << endl;
    this_thread::sleep_for(chrono::milliseconds(rand() % 3000));
    cout << "BT#" << id << " ends..." << endl;
}

int main()
{
    ThreadPool pool(8);

    pool.submit([] { background_task(1); }); // void()
    pool.submit([] { background_task(2); });

    for (int i = 3; i <= 20; ++i)
        pool.submit(bind(&background_task, i)); // void()
}
